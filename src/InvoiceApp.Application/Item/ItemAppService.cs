﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
 
using InvoiceApp.Dtos;
using InvoiceApp.Item.Dto;
using Microsoft.EntityFrameworkCore;

namespace InvoiceApp.Item
{
    public class ItemAppService : AsyncCrudAppService<Item, ItemDto, int, PagedResultRequestDto, CreateItemDto, ItemDto>, IItemAppService
    {
        public ItemAppService(IRepository<Item, int> repository) : base(repository)
        {
        }

        public async Task<GetItemListOutput> GetItemList()
        {
            var result = await Repository.GetAll().Select(t => new DropList()
            {
                Id = t.Id,
                Text = t.Name
            }).ToListAsync();
            return new GetItemListOutput()
            {
                DropLists = result
            };
        }
    }
}
