﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using InvoiceApp.Item.Dto;

namespace InvoiceApp.Item
{
   public interface IItemAppService : IAsyncCrudAppService<ItemDto, int, PagedResultRequestDto, CreateItemDto, ItemDto>
   {
       Task<GetItemListOutput> GetItemList();

   }
}
