﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace InvoiceApp.Item.Dto
{
    [AutoMap(typeof(Item))]
    public class ItemDto : EntityDto 
    {
        public string Name { get; set; }
       
    }
}
