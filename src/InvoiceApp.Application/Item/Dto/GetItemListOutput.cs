﻿using System;
using System.Collections.Generic;
using System.Text;
using InvoiceApp.Dtos;

namespace InvoiceApp.Item.Dto
{
  public  class GetItemListOutput
    {
        public List<DropList> DropLists { get; set; }
    }
}
