﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.AutoMapper;

namespace InvoiceApp.Item.Dto
{
    [AutoMapTo(typeof(Item))]
   public class CreateItemDto
    {
        public string Name { get; set; }
       

    }
}
