﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;

using InvoiceApp.Dtos;
using InvoiceApp.Invoice.Dto;
using InvoiceApp.InvoiceDetail;
using Microsoft.EntityFrameworkCore;

namespace InvoiceApp.Invoice
{
    public class InvoiceAppService : AsyncCrudAppService<Invoice, InvoiceDto, long, PagedResultRequestDto, CreateInvoiceDto, InvoiceDto>, IInvoiceAppService
    {
        private readonly IInvoiceDetailAppService _invoiceDetailAppService;

        public InvoiceAppService(IRepository<Invoice, long> repository, IInvoiceDetailAppService invoiceDetailAppService) : base(repository)
        {
            _invoiceDetailAppService = invoiceDetailAppService;
        }

        public override async Task<InvoiceDto> Create(CreateInvoiceDto input)
        {
            var invoice = await base.Create(input);
            foreach (var detail in input.CreateInvoiceDetailDtos)
            {
                detail.InvoiceId = invoice.Id;
                await _invoiceDetailAppService.Create(detail);
            }

            return invoice;
        }

        public async Task DeleteAll()
        {
            await _invoiceDetailAppService.DeleteAll();
            await Repository.DeleteAsync(t => t.Id > 0);
        }
    }
}
