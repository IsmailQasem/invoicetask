﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using InvoiceApp.Invoice.Dto;

namespace InvoiceApp.Invoice
{
   public interface IInvoiceAppService : IAsyncCrudAppService<InvoiceDto, long, PagedResultRequestDto, CreateInvoiceDto, InvoiceDto>
   {
       Task DeleteAll();


    }
}
