﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace InvoiceApp.Invoice.Dto
{
    [AutoMap(typeof(Invoice))]
    public class InvoiceDto : EntityDto<long>
    {
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public int StoreId { get; set; }

        public decimal Total { get; set; }
        public decimal Taxes { get; set; }
        public decimal Net { get; set; }

    }
}
