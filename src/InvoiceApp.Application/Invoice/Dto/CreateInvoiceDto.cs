﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.AutoMapper;
using InvoiceApp.InvoiceDetail.Dto;

namespace InvoiceApp.Invoice.Dto
{
    [AutoMapTo(typeof(Invoice))]
   public class CreateInvoiceDto
    {
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public int StoreId { get; set; }
     
        public decimal Total { get; set; }
        public decimal Taxes { get; set; }
        public decimal Net { get; set; }

        public List<CreateInvoiceDetailDto> CreateInvoiceDetailDtos { get; set; }
    }
}
