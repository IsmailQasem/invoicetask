﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InvoiceApp.Dtos
{
  public  class DropList
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
    public class LongDropList
    {
        public long Id { get; set; }
        public string Text { get; set; }
    }
    public class TxtDropList
    {
        public string Id { get; set; }
        public string Text { get; set; }
    }
}
