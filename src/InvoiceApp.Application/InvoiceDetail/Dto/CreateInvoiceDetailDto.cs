﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.AutoMapper;

namespace InvoiceApp.InvoiceDetail.Dto
{
    [AutoMapTo(typeof(InvoiceDetail))]
   public class CreateInvoiceDetailDto
    {
        public long InvoiceId { get; set; }
        
        public int ItemId { get; set; }
         
        public int UnitId { get; set; }
       
        public decimal Price { get; set; }
        public int Qty { get; set; }
        public decimal Total { get; set; }
        public decimal Discount { get; set; }
        public decimal Net { get; set; }
    }
}
