﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace InvoiceApp.InvoiceDetail.Dto
{
    [AutoMap(typeof(InvoiceDetail))]
    public class InvoiceDetailDto : EntityDto<long>
    {
        public long InvoiceId { get; set; }

        public int ItemId { get; set; }

        public int UnitId { get; set; }

        public decimal Price { get; set; }
        public int Qty { get; set; }
        public decimal Total { get; set; }
        public decimal Discount { get; set; }
        public decimal Net { get; set; }

    }
}
