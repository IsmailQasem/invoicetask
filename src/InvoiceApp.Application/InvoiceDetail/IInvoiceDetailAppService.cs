﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using InvoiceApp.InvoiceDetail.Dto;

namespace InvoiceApp.InvoiceDetail
{
   public interface IInvoiceDetailAppService : IAsyncCrudAppService<InvoiceDetailDto, long, PagedResultRequestDto, CreateInvoiceDetailDto, InvoiceDetailDto>
   {
       Task DeleteAll();

   }
}
