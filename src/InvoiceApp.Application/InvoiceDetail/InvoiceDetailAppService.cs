﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using InvoiceApp.InvoiceDetail.Dto;

namespace InvoiceApp.InvoiceDetail
{
    public class InvoiceDetailAppService : AsyncCrudAppService<InvoiceDetail, InvoiceDetailDto, long, PagedResultRequestDto, CreateInvoiceDetailDto, InvoiceDetailDto>, IInvoiceDetailAppService
    {
        public InvoiceDetailAppService(IRepository<InvoiceDetail, long> repository) : base(repository)
        {
        }

        public async Task DeleteAll()
        {
            await Repository.DeleteAsync(t => t.Id > 0);
        }
    }
}
