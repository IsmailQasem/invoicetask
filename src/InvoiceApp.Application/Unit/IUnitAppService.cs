﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using InvoiceApp.Unit.Dto;

namespace InvoiceApp.Unit
{
   public interface IUnitAppService : IAsyncCrudAppService<UnitDto, int, PagedResultRequestDto, CreateUnitDto, UnitDto>
   {
       Task<GetUnitListOutput> GetUnitList();

   }
}
