﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace InvoiceApp.Unit.Dto
{
    [AutoMap(typeof(Unit))]
    public class UnitDto : EntityDto 
    {
        public string Name { get; set; }
       
    }
}
