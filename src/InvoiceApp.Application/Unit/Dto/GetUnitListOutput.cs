﻿using System;
using System.Collections.Generic;
using System.Text;
using InvoiceApp.Dtos;

namespace InvoiceApp.Unit.Dto
{
  public  class GetUnitListOutput
    {
        public List<DropList> DropLists { get; set; }
    }
}
