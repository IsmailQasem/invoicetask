﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.AutoMapper;

namespace InvoiceApp.Unit.Dto
{
    [AutoMapTo(typeof(Unit))]
   public class CreateUnitDto
    {
        public string Name { get; set; }
       

    }
}
