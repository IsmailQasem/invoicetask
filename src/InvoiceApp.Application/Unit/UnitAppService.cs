﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
 
using InvoiceApp.Dtos;
using InvoiceApp.Unit.Dto;
using Microsoft.EntityFrameworkCore;

namespace InvoiceApp.Unit
{
    public class UnitAppService : AsyncCrudAppService<Unit, UnitDto, int, PagedResultRequestDto, CreateUnitDto, UnitDto>, IUnitAppService
    {
        public UnitAppService(IRepository<Unit, int> repository) : base(repository)
        {
        }

        public async Task<GetUnitListOutput> GetUnitList()
        {
            var result = await Repository.GetAll().Select(t => new DropList()
            {
                Id = t.Id,
                Text = t.Name
            }).ToListAsync();
            return new GetUnitListOutput()
            {
                DropLists = result
            };
        }
    }
}
