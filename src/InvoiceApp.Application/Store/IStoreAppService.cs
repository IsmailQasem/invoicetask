﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using InvoiceApp.Store.Dto;

namespace InvoiceApp.Store
{
   public interface IStoreAppService : IAsyncCrudAppService<StoreDto, int, PagedResultRequestDto, CreateStoreDto, StoreDto>
   {
       Task<GetStoreListOutput> GetStoreList();

   }
}
