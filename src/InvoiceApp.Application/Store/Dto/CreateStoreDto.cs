﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.AutoMapper;

namespace InvoiceApp.Store.Dto
{
    [AutoMapTo(typeof(Store))]
   public class CreateStoreDto
    {
        public string Name { get; set; }
       

    }
}
