﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace InvoiceApp.Store.Dto
{
    [AutoMap(typeof(Store))]
    public class StoreDto : EntityDto 
    {
        public string Name { get; set; }
       
    }
}
