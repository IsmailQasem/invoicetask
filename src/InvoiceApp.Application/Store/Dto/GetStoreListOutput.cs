﻿using System;
using System.Collections.Generic;
using System.Text;
using InvoiceApp.Dtos;

namespace InvoiceApp.Store.Dto
{
  public  class GetStoreListOutput
    {
        public List<DropList> DropLists { get; set; }
    }
}
