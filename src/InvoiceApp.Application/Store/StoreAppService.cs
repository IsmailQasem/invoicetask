﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
 using InvoiceApp.Dtos;
using InvoiceApp.Store.Dto;
using Microsoft.EntityFrameworkCore;

namespace InvoiceApp.Store
{
    public class StoreAppService : AsyncCrudAppService<Store, StoreDto, int, PagedResultRequestDto, CreateStoreDto, StoreDto>, IStoreAppService
    {
        public StoreAppService(IRepository<Store, int> repository) : base(repository)
        {
        }

        public async Task<GetStoreListOutput> GetStoreList()
        {
            var result = await Repository.GetAll().Select(t => new DropList()
            {
                Id = t.Id,
                Text = t.Name
            }).ToListAsync();
            return new GetStoreListOutput()
            {
                DropLists = result
            };
        }
    }
}
