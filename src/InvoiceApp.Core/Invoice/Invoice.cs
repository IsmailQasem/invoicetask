﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace InvoiceApp.Invoice
{
    [Table("Invoice",Schema = "dbo")]
   public class Invoice:Entity<long>
    {
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public int StoreId { get; set; }
        [ForeignKey("StoreId")]
        public virtual Store.Store Store{ get; set; }
        public decimal Total { get; set; }
        public decimal Taxes { get; set; }
        public decimal Net { get; set; }

    }
}
