﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace InvoiceApp.InvoiceDetail
{
    [Table("InvoiceDetail", Schema = "dbo")]
    public class InvoiceDetail : Entity<long>
    {
        public long InvoiceId { get; set; }
        [ForeignKey("InvoiceId")]
        public virtual Invoice.Invoice Invoice { get; set; }
        public int ItemId { get; set; }
        [ForeignKey("ItemId")]
        public virtual Item.Item Item { get; set; }
        public int UnitId { get; set; }
        [ForeignKey("UnitId")]
        public virtual Unit.Unit Unit { get; set; }
        public decimal Price { get; set; }
        public int Qty { get; set; }
        public decimal Total { get; set; }
        public decimal Discount { get; set; }
        public decimal Net { get; set; }

    }
}
