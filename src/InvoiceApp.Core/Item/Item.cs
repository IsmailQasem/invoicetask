﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace InvoiceApp.Item
{
    [Table("Item",Schema = "dbo")]
    public class Item : Entity
    {
        public string Name { get; set; }
    }
}
