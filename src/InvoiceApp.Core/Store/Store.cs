﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace InvoiceApp.Store
{
    [Table("Store", Schema = "dbo")]
    public class Store : Entity
    {
        public string Name { get; set; }
    }
}
