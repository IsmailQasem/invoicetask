﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Abp.Domain.Entities;

namespace InvoiceApp.Unit
{
    [Table("Unit", Schema = "dbo")]
    public class Unit : Entity
    {
        public string Name { get; set; }
    }
}
