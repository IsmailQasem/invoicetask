﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using InvoiceApp.Authorization.Roles;
using InvoiceApp.Authorization.Users;
using InvoiceApp.MultiTenancy;

namespace InvoiceApp.EntityFrameworkCore
{
    public class InvoiceAppDbContext : AbpZeroDbContext<Tenant, Role, User, InvoiceAppDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public virtual DbSet<Item.Item> Items { get; set; }
        public virtual DbSet<Unit.Unit> Units { get; set; }
        public virtual DbSet<Store.Store> Stores { get; set; }
        public virtual DbSet<Invoice.Invoice> Invoices { get; set; }
        public virtual DbSet<InvoiceDetail.InvoiceDetail> InvoiceDetails { get; set; }
        public InvoiceAppDbContext(DbContextOptions<InvoiceAppDbContext> options)
            : base(options)
        {
        }
    }
}
