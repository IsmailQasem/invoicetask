using System.Linq;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Editions;
using Abp.Application.Features;
using InvoiceApp.Editions;

namespace InvoiceApp.EntityFrameworkCore.Seed.Host
{
    public class DefaultInitCreator
    {
        private readonly InvoiceAppDbContext _context;

        public DefaultInitCreator(InvoiceAppDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateEditions();
        }

        private void CreateEditions()
        {
            if (_context.Items.IgnoreQueryFilters().Any())
            {
                return;
            }

            _context.Items.Add(new Item.Item()
            {
                
                Name = "Item 1"
            });
            _context.Items.Add(new Item.Item()
            {

                Name = "Item 2"
            });
            _context.Stores.Add(new Store.Store()
            {
               
                Name = "Store 1"
            });
            _context.Stores.Add(new Store.Store()
            {

                Name = "Store 2"
            });
            _context.Units.Add(new Unit.Unit()
            {
               
                Name = "Unit 1"
            });
            _context.Units.Add(new Unit.Unit()
            {

                Name = "Unit 2"
            });
            _context.SaveChanges();
        }

        
    }
}
