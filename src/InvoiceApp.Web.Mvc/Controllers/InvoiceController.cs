﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using InvoiceApp.Controllers;
using InvoiceApp.Item;
using InvoiceApp.Store;
using InvoiceApp.Unit;
using InvoiceApp.Web.Models.Invoice;

namespace InvoiceApp.Web.Controllers
{
    [AbpMvcAuthorize]
    public class InvoiceController : InvoiceAppControllerBase
    {
        private readonly IStoreAppService _storeAppService;
        private readonly IUnitAppService _unitAppService;
        private readonly IItemAppService _itemAppService;

        public InvoiceController(IStoreAppService storeAppService, IUnitAppService unitAppService, IItemAppService itemAppService)
        {
            _storeAppService = storeAppService;
            _unitAppService = unitAppService;
            _itemAppService = itemAppService;
        }

        public async Task<ActionResult> Index()
        {
            var stores = await _storeAppService.GetStoreList();
            var unites = await _unitAppService.GetUnitList();
            var items = await _itemAppService.GetItemList();
            var model = new InvoiceModel()
            {
                UnitDropLists = unites.DropLists,
                ItemDropLists = items.DropLists,
                StoreDropLists = stores.DropLists
            };
            return View(model);
        }
	}
}
