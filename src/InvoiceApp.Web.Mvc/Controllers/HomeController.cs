﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using InvoiceApp.Controllers;

namespace InvoiceApp.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : InvoiceAppControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
