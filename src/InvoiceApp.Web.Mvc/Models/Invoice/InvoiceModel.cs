﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InvoiceApp.Dtos;

namespace InvoiceApp.Web.Models.Invoice
{
    public class InvoiceModel
    {
        public List<DropList> StoreDropLists { get; set; }
        public List<DropList> UnitDropLists { get; set; }
        public List<DropList> ItemDropLists { get; set; }
    }
}
