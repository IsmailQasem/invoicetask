﻿using System.Collections.Generic;
using InvoiceApp.Roles.Dto;

namespace InvoiceApp.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleListDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
