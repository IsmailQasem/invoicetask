﻿using Abp.AutoMapper;
using InvoiceApp.Roles.Dto;
using InvoiceApp.Web.Models.Common;

namespace InvoiceApp.Web.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class EditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool HasPermission(FlatPermissionDto permission)
        {
            return GrantedPermissionNames.Contains(permission.Name);
        }
    }
}
