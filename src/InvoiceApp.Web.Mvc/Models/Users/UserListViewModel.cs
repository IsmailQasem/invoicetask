using System.Collections.Generic;
using InvoiceApp.Roles.Dto;
using InvoiceApp.Users.Dto;

namespace InvoiceApp.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
