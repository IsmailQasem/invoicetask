﻿using System.Collections.Generic;
using InvoiceApp.Roles.Dto;

namespace InvoiceApp.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}