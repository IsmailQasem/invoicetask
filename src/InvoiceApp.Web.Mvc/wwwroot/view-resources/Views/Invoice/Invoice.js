﻿var source = abp.localization.getSource('InvoiceApp');
var table;
var InvoiceService = abp.services.app.invoice;
 
var $form = $('#InvoiceCreateForm');

var invoiceArr = [];

 
function InitProduct() {
    $('.datepicker').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        clearButton: true,
        weekStart: 1,
        time: false
    });
}

InitProduct();
(function () {
    $(function () {

        $form.validate({
            rules: {
                Name: "required"
            }
        });

        $form.find('button[type="submit"]').click(function (e) {
            e.preventDefault();

            if (!$form.valid()) {
                return;
            }

            var invoice = $form.serializeFormToObject();
            invoice.Total = $("#InvoiceTotal").val();
            invoice.Taxes = $("#InvoiceTaxes").val();
            invoice.Net = $("#InvoiceNet").val();
            invoice.CreateInvoiceDetailDtos = invoiceArr;
            abp.ui.setBusy($form);

            InvoiceService.create(invoice).done(function () {
 
                location.reload(true); //reload page to see new user!
            }).always(function () {
                abp.ui.clearBusy($form);
            });


        });
        

    });
})();

 

function loadDatatableTable() {
    var index = 0;
    $(invoiceArr).each(function () {
        this.index = index;
        index++;
    });
    $('#tbInvoice').dataTable().fnClearTable();
    $('#tbInvoice').dataTable().fnAddData(invoiceArr);

    $("select[name='Item']").on('change', function () {
        var index = $(this).attr('index');
        var rec = invoiceArr[index];

        if (rec != null) {
            rec.ItemId = $(this).val();

        }

    }); $("select[name='Unit']").on('change', function () {
        var index = $(this).attr('index');
        var rec = invoiceArr[index];

        if (rec != null) {
            rec.UnitId = $(this).val();

        }

    });
    $("input[name='Price']").on('keyup', function () {
        var index = $(this).attr('index');
        var rec = invoiceArr[index];

        if (rec != null) {
            rec.Price = $(this).val();

        }
        calcRecordSummary(rec, index);
    });
    $("input[name='Qty']").on('keyup', function () {
        var index = $(this).attr('index');
        var rec = invoiceArr[index];
        if (rec != null) {
            rec.Qty = $(this).val();

        }
        calcRecordSummary(rec, index);
    });
    $("input[name='Discount']").on('keyup', function () {
        var index = $(this).attr('index');
        var rec = invoiceArr[index];

        if (rec != null) {
            rec.Discount = $(this).val();

        }
        calcRecordSummary(rec, index);
    });
    $("select[name='Item']").each(function () {
        var value = $(this).attr('value');
        $(this).val(value);

    });
    $("select[name='Unit']").each(function () {
        var value = $(this).attr('value');
        $(this).val(value);

    });

}


var datatable;


function InitTable() {
    datatable = $('#tbInvoice');

    datatable.DataTable({
        "bPaginate": false,
        columns: [
            { data: 'ItemId' },
            { data: 'UnitId' },
            { data: 'Price' },
            { data: 'Qty' },
            { data: 'Total' },
            { data: 'Discount' },
            { data: 'Net' }


        ]

        , columnDefs: [
            {
                targets: [0],
                render: function (data, type, full, meta) {

                    return buildCmb("Item", data, full.index);


                }
            },
            {
                targets: [1],
                render: function (data, type, full, meta) {

                    return buildCmb("Unit", data, full.index);


                }
            },
            {
                targets: [2],
                render: function (data, type, full, meta) {

                    return '<input style="width:100px" name="Price" type="number" value="' + data + '" id="Price_' + full.index + '" index=' + full.index + ' class="form-control" required/>';


                }
            },
            {
                targets: [3],
                render: function (data, type, full, meta) {

                    return '<input style="width:100px" name="Qty" type="number" value="' + data + '" id="Qty_' + full.index + '" index=' + full.index + ' class="form-control" required/>';


                }
            },
            {
                targets: [4],
                render: function (data, type, full, meta) {

                    return '<input style="width:100px" name="Total" type="number" disabled="disabled" value="' + data + '" id="Total_' + full.index + '" index=' + full.index + ' class="form-control" required/>';


                }
            },
            {
                targets: [5],
                render: function (data, type, full, meta) {

                    return '<input style="width:100px" name="Discount" type="number" value="' + data + '" id="Discount_' + full.index + '" index=' + full.index + ' class="form-control" required/>';


                }
            },
            {
                targets: [6],
                render: function (data, type, full, meta) {

                    return '<input style="width:100px" name="Net" type="number" disabled="disabled" value="' + data + '" id="Net_' + full.index + '" index=' + full.index + ' class="form-control" required/>';


                }
            }
        ]
        ,
        responsive: true

    });

}

InitTable();


$("#btnAdd").on('click',
    function () {
        invoiceArr.push({
            ItemId: null,
            UnitId: null,
            Price: 0,
            Qty: 0,
            Total: 0,
            Discount: 0,
            Net: 0

        });
        loadDatatableTable();
    });
 
function buildCmb(name, value, index) {
    var cmb = '<select value="' + value + '" class="form-control show-tick" id="' + name + '' +
        index +
        '" required name="' + name + '" data-live-search="true" index=' + index + '>' +
        '  <option></option>';
    switch (name) {
        case "Item":
            cmb = cmb + '<option value="1">Item 1</option><option value="2">Item 2</option></select>';
            break;
        case "Unit":
            cmb = cmb + '<option value="1">Unit 1</option><option value="2">Unit 2</option></select>';
            break;
    }

    return cmb;


}

function calcRecordSummary(rec,index) {
    var total = 0;

    if (rec.Qty >0 && rec.Price>0) {
        total = rec.Qty * rec.Price;
    }
    $("#Total_" + index).val(total);
    rec.Total = total;

    if (rec.Discount>0) {
        total = total - rec.Discount;
    }
    $("#Net_" + index).val(total);
    rec.Net = total;

    CalcAllSummary();

}

function CalcAllSummary() {
    var totalall = 0;

    $(invoiceArr).each(function () {
        totalall = totalall + this.Net;
    });

    $("#InvoiceTotal").val(totalall);
    if ($("#InvoiceTaxes").val() > 0) {
        totalall = totalall - (totalall * ($("#InvoiceTaxes").val() / 100));
    }
    $("#InvoiceNet").val(totalall);
}

$("#InvoiceTaxes").on('keyup', function() {
    CalcAllSummary();
});

$("#btnDeleteAll").click(function () {
    abp.ui.setBusy($form);

    InvoiceService.deleteAll().done(function () {
 
    }).always(function () {
        abp.ui.clearBusy($form);
    });
});