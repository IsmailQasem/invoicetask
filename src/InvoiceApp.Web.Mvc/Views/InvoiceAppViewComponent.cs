﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace InvoiceApp.Web.Views
{
    public abstract class InvoiceAppViewComponent : AbpViewComponent
    {
        protected InvoiceAppViewComponent()
        {
            LocalizationSourceName = InvoiceAppConsts.LocalizationSourceName;
        }
    }
}
