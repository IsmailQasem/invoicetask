﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace InvoiceApp.Web.Views
{
    public abstract class InvoiceAppRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected InvoiceAppRazorPage()
        {
            LocalizationSourceName = InvoiceAppConsts.LocalizationSourceName;
        }
    }
}
