﻿using Abp.Application.Navigation;
using Abp.Localization;
using InvoiceApp.Authorization;

namespace InvoiceApp.Web.Startup
{
    /// <summary>
    /// This class defines menus for the application.
    /// </summary>
    public class InvoiceAppNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(
                    new MenuItemDefinition(
                        PageNames.Home,
                        L("HomePage"),
                        url: "",
                        icon: "home",
                        requiresAuthentication: true
                    )
                ).AddItem(
                    new MenuItemDefinition(
                        PageNames.Invoice,
                        L("Invoice"),
                        url: "Invoice",
                        icon: "receipt",
                        requiresAuthentication: true
                    )
                ) ;
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, InvoiceAppConsts.LocalizationSourceName);
        }
    }
}
